import { call, put, takeEvery } from 'redux-saga/effects';

import * as api from '../api';
import { MODALS } from '../constants';
import * as actions from '../actions';

export function* addFolder({ parentId }) {
  try {
    const { folder, parent } = yield call(api.post, '/api/new/folder', { parentId });
    yield put(actions.addFolderSuccess(folder, parent));
  } catch (error) {
    yield put(actions.addFolderFailure(error.message));
  }
}

export function* addWork({ parentId }) {
  try {
    const { parent, work } = yield call(api.post, '/api/new/work', { parentId });
    yield put(actions.addWorkSuccess(work, parent));
  } catch (error) {
    yield put(actions.addWorkFailure(error.message));
  }
}

export function* deleteFolder({ folderId }) {
  try {
    const { parent } = yield call(api.del, `/api/delete/folder/${folderId}`);
    yield put(actions.deleteFolderSuccess(parent));
  } catch (error) {
    yield put(actions.deleteFolderFailure(error.message));
  }
}

export function* deleteWork({ workId }) {
  try {
    const { parent } = yield call(api.del, `/api/delete/work/${workId}`);
    yield put(actions.deleteWorkSuccess(parent));
  } catch (error) {
    yield put(actions.deleteWorkFailure(error.message));
  }
}

export function* editFolder({ folderId: id, spec }) {
  try {
    const { folder, parent } = yield call(api.put, '/api/update/folder', { id, spec });
    yield put(actions.editFolderSuccess(folder, parent));
  } catch (error) {
    yield put(actions.getFolderFailure(error.message));
  }
}

export function* editWork({ workId: id, spec }) {
  try {
    const toReturn = ['parent', 'work'];
    const { parent, work } = yield call(api.put, '/api/update/work', { id, spec, toReturn });
    yield put(actions.editWorkSuccess(work, parent));
  } catch (error) {
    yield put(actions.editWorkFailure(error.message));
  }
}

export function* editWorkMeta({ workId: id, spec }) {
  try {
    const toReturn = ['parent', 'work'];
    const { parent, work } = yield call(api.put, '/api/update/work', { id, spec, toReturn });
    yield put(actions.editWorkMetaSuccess(work, parent));
  } catch (error) {
    yield put(actions.editWorkMetaFailure(error.message));
  }
}

export function* editWorkQuick({ workId: id, spec }) {
  try {
    const toReturn = ['parent', 'work'];
    const { parent, work } = yield call(api.put, '/api/update/work', { id, spec, toReturn });
    yield put(actions.editWorkQuickSuccess(work, parent));
  } catch (error) {
    yield put(actions.editWorkQuickFailure(error.message));
  }
}

export function* getDrafts() {
  try {
    const { drafts } = yield call(api.get, '/api/works/drafts');
    yield put(actions.getDraftsSuccess(drafts));
  } catch (error) {
    yield put(actions.getDraftsFailure(error.message));
  }
}

export function* getFolder({ folderId }) {
  try {
    const { folder } = yield call(api.get, `/api/folder/${folderId}`);
    yield put(actions.getFolderSuccess(folder));
  } catch (error) {
    yield put(actions.getFolderFailure(error.message));
  }
}

export function* getPedigree() {
  try {
    const { pedigree } = yield call(api.get, '/api/folders/pedigree');
    yield put(actions.getFolderPedigreeSuccess(pedigree));
  } catch (error) {
    yield put(actions.getFolderPedigreeFailure(error.message));
  }
}

export function* getWork({ workId }) {
  try {
    const { work } = yield call(api.get, `/api/work/${workId}`);
    yield put(actions.getWorkSuccess(work));
    yield put(actions.setWork(work));
    yield put(actions.closeModal());
  } catch (error) {
    yield put(actions.getWorkFailure(error.message));
  }
}

export function* moveChild({ childId, folderId, isFolder }) {
  try {
    let response;
    const body = {
      id: childId,
      newParentId: folderId,
    };

    if (isFolder) {
      response = yield call(api.put, '/api/move/folder', body);
    } else {
      response = yield call(api.put, '/api/move/work', body);
    }

    const { newParentFolder } = response;

    yield put(actions.moveChildSuccess(newParentFolder));
    yield put(actions.openModal(MODALS.EXPLORER, { folderId: newParentFolder.id }));
  } catch (error) {
    yield put(actions.moveChildFailure(error.message));
  }
}

export function* updateActiveStatus({ active, id }) {
  try {
    const spec = { active };
    const toReturn = ['drafts'];
    const { drafts } = yield call(api.put, '/api/update/work', { id, spec, toReturn });
    yield put(actions.updateActiveStatusSuccess(drafts));
  } catch (error) {
    yield put(actions.updateActiveStatusFailure(error.message));
  }
}

export default function* rootSaga() {
  yield takeEvery('ADD_FOLDER_REQUEST', addFolder);
  yield takeEvery('ADD_WORK_REQUEST', addWork);
  yield takeEvery('DELETE_FOLDER_REQUEST', deleteFolder);
  yield takeEvery('DELETE_WORK_REQUEST', deleteWork);
  yield takeEvery('EDIT_FOLDER_REQUEST', editFolder);
  yield takeEvery('EDIT_WORK_REQUEST', editWork);
  yield takeEvery('EDIT_WORK_META_REQUEST', editWorkMeta);
  yield takeEvery('EDIT_WORK_QUICK_REQUEST', editWorkQuick);
  yield takeEvery('GET_DRAFTS_REQUEST', getDrafts);
  yield takeEvery('GET_FOLDER_REQUEST', getFolder);
  yield takeEvery('GET_PEDIGREE_REQUEST', getPedigree);
  yield takeEvery('GET_WORK_REQUEST', getWork);
  yield takeEvery('MOVE_CHILD_REQUEST', moveChild);
  yield takeEvery('UPDATE_ACTIVE_STATUS_REQUEST', updateActiveStatus);
}
