import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducers';
import sagas from './sagas';

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const middleware = [sagaMiddleware];

  if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger({
      collapsed: true,
    }));
  }

  const store = createStore(reducer, composeWithDevTools(applyMiddleware(...middleware)));

  let sagaTask = sagaMiddleware.run(sagas);

  if (module.hot) {
    module.hot.accept('./sagas', () => {
      sagaTask.cancel();
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(sagas);
      });
    });
    module.hot.accept('./reducers', () => store.replaceReducer(reducer));
  }

  return store;
};

export default configureStore;
