/* FIC STATE */

export const addFolder = parentId => ({
  type: 'ADD_FOLDER_REQUEST',
  parentId,
});

export const addFolderSuccess = (folder, parent) => ({
  type: 'ADD_FOLDER_SUCCESS',
  folder,
  parent,
});

export const addFolderFailure = message => ({
  type: 'ADD_FOLDER_FAILURE',
  message: message || 'Failed to add folder',
});

export const addWork = parentId => ({
  type: 'ADD_WORK_REQUEST',
  parentId,
});

export const addWorkSuccess = (work, parent) => ({
  type: 'ADD_WORK_SUCCESS',
  parent,
  work,
});

export const addWorkFailure = message => ({
  type: 'ADD_WORK_FAILURE',
  message: message || 'Failed to add Work',
});

export const deleteFolder = folderId => ({
  type: 'DELETE_FOLDER_REQUEST',
  folderId,
});

export const deleteFolderSuccess = parent => ({
  type: 'DELETE_FOLDER_SUCCESS',
  parent,
});

export const deleteFolderFailure = message => ({
  type: 'DELETE_FOLDER_FAILURE',
  message: message || 'Failed to delete folder',
});

export const deleteWork = workId => ({
  type: 'DELETE_WORK_REQUEST',
  workId,
});

export const deleteWorkSuccess = parent => ({
  type: 'DELETE_WORK_SUCCESS',
  parent,
});

export const deleteWorkFailure = message => ({
  type: 'DELETE_WORK_FAILURE',
  message: message || 'Failed to delete work',
});

export const editFolder = (folderId, spec) => ({
  type: 'EDIT_FOLDER_REQUEST',
  folderId,
  spec,
});

export const editFolderSuccess = (folder, parent) => ({
  type: 'EDIT_FOLDER_SUCCESS',
  folder,
  parent,
});

export const editFolderFailure = message => ({
  type: 'EDIT_FOLDER_FAILURE',
  message: message || 'Failed to edit folder',
});

export const editWork = (workId, spec) => ({
  type: 'EDIT_WORK_REQUEST',
  spec,
  workId,
});

export const editWorkSuccess = (work, parent) => ({
  type: 'EDIT_WORK_SUCCESS',
  parent,
  work,
});

export const editWorkFailure = message => ({
  type: 'EDIT_WORK_FAILURE',
  message: message || 'Failed to edit work',
});

export const editWorkMeta = (workId, spec) => ({
  type: 'EDIT_WORK_META_REQUEST',
  spec,
  workId,
});

export const editWorkMetaSuccess = (work, parent) => ({
  type: 'EDIT_WORK_META_SUCCESS',
  parent,
  work,
});

export const editWorkMetaFailure = message => ({
  type: 'EDIT_WORK_META_FAILURE',
  message: message || 'Failed to edit work',
});

export const editWorkQuick = (workId, spec) => ({
  type: 'EDIT_WORK_QUICK_REQUEST',
  spec,
  workId,
});

export const editWorkQuickSuccess = (work, parent) => ({
  type: 'EDIT_WORK_QUICK_SUCCESS',
  parent,
  work,
});

export const editWorkQuickFailure = message => ({
  type: 'EDIT_WORK_QUICK_FAILURE',
  message: message || 'Failed to edit work',
});

export const getDrafts = () => ({
  type: 'GET_DRAFTS_REQUEST',
});

export const getDraftsSuccess = drafts => ({
  type: 'GET_DRAFTS_SUCCESS',
  drafts,
});

export const getDraftsFailure = message => ({
  type: 'GET_DRAFTS_FAILURE',
  message: message || 'Failed to fetch drafts',
});

export const getFolder = folderId => ({
  type: 'GET_FOLDER_REQUEST',
  folderId,
});

export const getFolderSuccess = folder => ({
  type: 'GET_FOLDER_SUCCESS',
  folder,
});

export const getFolderFailure = message => ({
  type: 'GET_FOLDER_FAILURE',
  message: message || 'Something went wrong fetching the folder',
});

export const getFolderPedigree = () => ({
  type: 'GET_PEDIGREE_REQUEST',
});

export const getFolderPedigreeSuccess = pedigree => ({
  type: 'GET_PEDIGREE_SUCCESS',
  pedigree,
});

export const getFolderPedigreeFailure = message => ({
  type: 'GET_PEDIGREE_FAILURE',
  message: message || 'Failed to fetch folder pedigree',
});

export const getWork = workId => ({
  type: 'GET_WORK_REQUEST',
  workId,
});

export const getWorkSuccess = work => ({
  type: 'GET_WORK_SUCCESS',
  work,
});

export const getWorkFailure = message => ({
  type: 'GET_WORK_FAILURE',
  message: message || 'Failed to fetch work',
});

export const moveChild = (childId, folderId, isFolder) => ({
  type: 'MOVE_CHILD_REQUEST',
  childId,
  folderId,
  isFolder,
});

export const moveChildSuccess = parent => ({
  type: 'MOVE_CHILD_SUCCESS',
  parent,
});

export const moveChildFailure = message => ({
  type: 'MOVE_CHILD_FAILURE',
  message: message || 'Failed to move child',
});

export const updateActiveStatus = (id, active) => ({
  type: 'UPDATE_ACTIVE_STATUS_REQUEST',
  active,
  id,
});

export const updateActiveStatusSuccess = drafts => ({
  type: 'UPDATE_ACTIVE_STATUS_SUCCESS',
  drafts,
});

export const updateActiveStatusFailure = message => ({
  type: 'UPDATE_ACTIVE_STATUS_FAILURE',
  message: message || 'Failed to update active status',
});

/* FUNCTIONAL STATE */

export const openModal = (modalType, modalSpec = {}) => ({
  type: 'OPEN_MODAL',
  modalSpec,
  modalType,
});

export const closeModal = () => ({
  type: 'CLOSE_MODAL',
});

export const openNotes = () => ({
  type: 'OPEN_NOTES',
});

export const closeNotes = () => ({
  type: 'CLOSE_NOTES',
});

export const selectTab = tab => ({
  type: 'SELECT_TAB',
  tab,
});

export const setWork = work => ({
  type: 'SET_WORK',
  work,
});
