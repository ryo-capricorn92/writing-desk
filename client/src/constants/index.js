export const HOME = {
  ID: 'home',
  NAME: 'Home',
};

export const BREADCRUMB_DEFAULT = [{
  id: HOME.ID,
  name: HOME.NAME,
}];

export const EXPLORER_ACTIONS = {
  DELETE: 'delete',
  EDIT: 'edit',
};

export const MODALS = {
  EXPLORER: 'explorer',
  META: 'meta',
  MOVE: 'move',
};

export const OPTION_TEXT = {
  AUTO_SCROLL: {
    true: 'Turn off auto-scroll',
    false: 'Turn on auto-scroll',
  },
};
