export const AO3ToQuill = raw => raw
  .replace(/&lt;/g, '<') // use real less than
  .replace(/&gt;/g, '>') // use real greater than
  .replace(/<(p|i|b)>(<br>)*<\/(\1)>/g, '') // no empty tags/blank lines
  .replace(/<br \/><hr \/><br \/>/g, '~~~'); // use fake page breaks

export const quillToWordcounter = raw => raw
  .replace(/<(\/?)(em|strong)>/g, (found, bs, el) => `<${bs}${el === 'em' ? 'i' : 'b'}>`);

export const quillToAO3 = raw => raw
  .replace(/&nbsp;/g, ' ') // use raw whitespaces
  .replace(/<(\/?)em>/g, (found, el) => `<${el}i>`) // use i over em
  .replace(/<(\/?)strong>/g, (found, el) => `<${el}b>`) // use b over stronge
  .replace(/<(p|i|b)>\s/g, (found, el) => ` <${el}>`) // whitespace should go before opening tag
  .replace(/\s<\/(p|i|b)>/g, (found, el) => `<${el}> `) // whitespace should go after opening tag
  .replace(/<(p|i|b)>(<br>)*<\/(\1)>/g, '') // no empty tags/blank lines
  .replace(/<\/p><p>/g, `</p>

<p>`) // new lines between paragraphs are aesthetically pleasing
  .replace(/<p>~+<\/p>/g, '<br /><hr /><br />'); // use real page breaks

export const sortChildren = (a, b) => {
  // folders first
  if (a.isFolder && !b.isFolder) { return -1; }
  if (b.isFolder && !a.isFolder) { return 1; }
  // drafts before completed
  if (a.draft && !b.draft) { return -1; }
  if (b.draft && !a.draft) { return 1; }
  // active drafts before inactive drafts
  if ((a.draft && a.active) && (b.draft && !b.active)) { return -1; }
  if ((b.draft && b.active) && (a.draft && !a.active)) { return 1; }
  // finally, sort alphabetically
  if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
  if (b.name.toLowerCase() < a.name.toLowerCase()) { return 1; }
  return 0;
};
