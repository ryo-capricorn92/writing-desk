const options = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
};

export const del = url => (
  fetch(url, { ...options, method: 'DELETE' })
    .then(res => res.json())
);

export const get = url => (
  fetch(url, options)
    .then(res => res.json())
);

export const post = (url, payload) => (
  fetch(url, { ...options, method: 'POST', body: JSON.stringify(payload) })
    .then(res => res.json())
);

export const put = (url, payload) => (
  fetch(url, { ...options, method: 'PUT', body: JSON.stringify(payload) })
    .then(res => res.json())
);
