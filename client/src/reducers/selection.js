import { combineReducers } from 'redux';

const tab = (state = 0, action) => {
  switch (action.type) {
    case 'SELECT_TAB':
      return action.tab;
    case 'CLOSE_MODAL':
      return 0;
    default:
      return state;
  }
};

const selection = combineReducers({
  tab,
});

export default selection;

export const getTab = state => state.tab;
