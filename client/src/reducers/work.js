const work = (state = {}, action) => {
  switch (action.type) {
    case 'GET_WORK_SUCCESS':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_META_SUCCESS':
      return action.work;
    default:
      return state;
  }
};

export default work;
