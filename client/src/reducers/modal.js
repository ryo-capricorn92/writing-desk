import { combineReducers } from 'redux';

const active = (state = false, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return true;
    case 'CLOSE_MODAL':
    case 'EDIT_WORK_META_SUCCESS':
      return false;
    default:
      return state;
  }
};

const spec = (state = {}, action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return action.modalSpec;
    case 'CLOSE_MODAL':
      return {};
    default:
      return state;
  }
};

const type = (state = '', action) => {
  switch (action.type) {
    case 'OPEN_MODAL':
      return action.modalType;
    case 'CLOSE_MODAL':
      return '';
    default:
      return state;
  }
};

const modal = combineReducers({
  active,
  spec,
  type,
});

export default modal;

export const getModalActive = state => state.active;
export const getModalSpec = state => state.spec;
export const getModalType = state => state.type;
