const loading = (state = false, action) => {
  switch (action.type) {
    case 'ADD_FOLDER_REQUEST':
    case 'ADD_WORK_REQUEST':
    case 'DELETE_FOLDER_REQUEST':
    case 'DELETE_WORK_REQUEST':
    case 'EDIT_FOLDER_REQUEST':
    case 'EDIT_WORK_REQUEST':
    case 'EDIT_WORK_META_REQUEST':
    case 'EDIT_WORK_QUICK_REQUEST':
    case 'GET_FOLDER_REQUEST':
    case 'GET_PEDIGREE_REQUEST':
    case 'GET_WORK_REQUEST':
    case 'UPDATE_ACTIVE_STATUS_REQUEST':
      return true;
    case 'ADD_FOLDER_SUCCESS':
    case 'ADD_FOLDER_FAILURE':
    case 'ADD_WORK_SUCCESS':
    case 'ADD_WORK_FAILURE':
    case 'DELETE_FOLDER_SUCCESS':
    case 'DELETE_FOLDER_FAILURE':
    case 'DELETE_WORK_SUCCESS':
    case 'DELETE_WORK_FAILURE':
    case 'EDIT_FOLDER_SUCCESS':
    case 'EDIT_FOLDER_FAILURE':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_FAILURE':
    case 'EDIT_WORK_META_SUCESS':
    case 'EDIT_WORK_META_FAILURE':
    case 'EDIT_WORK_QUICK_SUCCESS':
    case 'EDIT_WORK_QUICK_FAILURE':
    case 'GET_FOLDER_SUCCESS':
    case 'GET_FOLDER_FAILURE':
    case 'GET_PEDIGREE_SUCCESS':
    case 'GET_PEDIGREE_FAILURE':
    case 'GET_WORK_SUCCESS':
    case 'GET_WORK_FAILURE':
    case 'UPDATE_ACTIVE_STATUS_SUCCESS':
    case 'UPDATE_ACTIVE_STATUS_FAILURE':
      return false;
    default:
      return state;
  }
};

export default loading;
