const saving = (state = false, action) => {
  switch (action.type) {
    case 'EDIT_FOLDER_REQUEST':
    case 'EDIT_WORK_REQUEST':
      return true;
    case 'EDIT_FOLDER_SUCCESS':
    case 'EDIT_FOLDER_FAILURE':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_FAILURE':
      return false;
    default:
      return state;
  }
};

export default saving;
