import { combineReducers } from 'redux';

const active = (state = false, action) => {
  switch (action.type) {
    case 'ADD_FOLDER_FAILURE':
    case 'ADD_WORK_FAILURE':
    case 'DELETE_FOLDER_FAILURE':
    case 'DELETE_WORK_FAILURE':
    case 'EDIT_FOLDER_FAILURE':
    case 'EDIT_WORK_FAILURE':
    case 'EDIT_WORK_META_FAILURE':
    case 'EDIT_WORK_QUICK_FAILURE':
    case 'GET_FOLDER_FAILURE':
    case 'GET_PEDIGREE_FAILURE':
    case 'GET_WORK_FAILURE':
    case 'UPDATE_ACTIVE_STATUS_FAILURE':
      return true;
    case 'ADD_FOLDER_SUCCESS':
    case 'ADD_WORK_SUCCESS':
    case 'DELETE_FOLDER_SUCCESS':
    case 'DELETE_WORK_SUCCESS':
    case 'EDIT_FOLDER_SUCCESS':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_META_SUCCESS':
    case 'EDIT_WORK_QUICK_SUCCESS':
    case 'GET_FOLDER_SUCCESS':
    case 'GET_PEDIGREE_SUCCESS':
    case 'GET_WORK_SUCCESS':
    case 'UPDATE_ACTIVE_STATUS_SUCCESS':
      return false;
    default:
      return state;
  }
};

const message = (state = '', action) => {
  switch (action.type) {
    case 'ADD_FOLDER_FAILURE':
    case 'ADD_WORK_FAILURE':
    case 'DELETE_FOLDER_FAILURE':
    case 'DELETE_WORK_FAILURE':
    case 'EDIT_FOLDER_FAILURE':
    case 'EDIT_WORK_FAILURE':
    case 'EDIT_WORK_META_FAILURE':
    case 'EDIT_WORK_QUICK_FAILURE':
    case 'GET_FOLDER_FAILURE':
    case 'GET_PEDIGREE_FAILURE':
    case 'GET_WORK_FAILURE':
    case 'UPDATE_ACTIVE_STATUS_FAILURE':
      return action.message;
    case 'ADD_FOLDER_SUCCESS':
    case 'ADD_WORK_SUCCESS':
    case 'DELETE_FOLDER_SUCCESS':
    case 'DELETE_WORK_SUCCESS':
    case 'EDIT_FOLDER_SUCCESS':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_META_SUCCESS':
    case 'EDIT_WORK_QUICK_SUCCESS':
    case 'GET_FOLDER_SUCCESS':
    case 'GET_PEDIGREE_SUCCESS':
    case 'GET_WORK_SUCCESS':
    case 'UPDATE_ACTIVE_STATUS_SUCCESS':
      return '';
    default:
      return state;
  }
};

const error = combineReducers({
  active,
  message,
});

export default error;

export const getError = state => state.active;
export const getErrorMessage = state => state.message;
