const drafts = (state = null, action) => {
  switch (action.type) {
    case 'GET_DRAFTS_SUCCESS':
    case 'UPDATE_ACTIVE_STATUS_SUCCESS':
      return action.drafts;
    case 'CLOSE_MODAL':
      return null;
    default:
      return state;
  }
};

export default drafts;
