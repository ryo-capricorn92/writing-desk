const pedigree = (state = {}, action) => {
  switch (action.type) {
    case 'GET_PEDIGREE_SUCCESS':
      return action.pedigree;
    case 'CLOSE_MODAL':
      return {};
    default:
      return state;
  }
};

export default pedigree;
