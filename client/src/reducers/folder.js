const folder = (state = {}, action) => {
  switch (action.type) {
    case 'GET_FOLDER_SUCCESS':
      return action.folder;
    case 'ADD_FOLDER_SUCCESS':
    case 'ADD_WORK_SUCCESS':
    case 'DELETE_FOLDER_SUCCESS':
    case 'DELETE_WORK_SUCCESS':
    case 'EDIT_FOLDER_SUCCESS':
    case 'EDIT_WORK_SUCCESS':
    case 'EDIT_WORK_QUICK_SUCCESS':
    case 'MOVE_CHILD_SUCCESS':
      if (state.id) {
        return action.parent;
      }
      return state;
    case 'CLOSE_MODAL':
      return {};
    default:
      return state;
  }
};

export default folder;
