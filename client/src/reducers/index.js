import { combineReducers } from 'redux';

import drafts from './drafts';
import folder from './folder';
import loading from './loading';
import notes from './notes';
import pedigree from './pedigree';
import saving from './saving';
import work from './work';

import error, * as fromError from './error';
import modal, * as fromModal from './modal';
import selection, * as fromSelection from './selection';

export default combineReducers({
  drafts,
  error,
  folder,
  loading,
  modal,
  notes,
  pedigree,
  saving,
  selection,
  work,
});

export const getDrafts = state => state.drafts;
export const getFolder = state => state.folder;
export const getLoading = state => state.loading;
export const getNotes = state => state.notes;
export const getPedigree = state => state.pedigree;
export const getSaving = state => state.saving;
export const getWork = state => state.work;

export const getError = state => fromError.getError(state.error);
export const getErrorMessage = state => fromError.getErrorMessage(state.error);
export const getModalActive = state => fromModal.getModalActive(state.modal);
export const getModalSpec = state => fromModal.getModalSpec(state.modal);
export const getModalType = state => fromModal.getModalType(state.modal);
export const getSelectionTab = state => fromSelection.getTab(state.selection);
