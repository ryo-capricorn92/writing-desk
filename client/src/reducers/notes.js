const notes = (state = false, action) => {
  switch (action.type) {
    case 'OPEN_NOTES':
      return true;
    case 'CLOSE_NOTES':
      return false;
    default:
      return state;
  }
};

export default notes;
