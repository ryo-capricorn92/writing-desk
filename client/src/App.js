/* eslint-disable react/jsx-filename-extension */
import PropTypes from 'prop-types';
import React from 'react';
import ReactQuill from 'react-quill';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from './actions';
import { HOME, MODALS } from './constants';
import { getModalActive, getSaving, getWork } from './reducers';
import { AO3ToQuill, quillToWordcounter, quillToAO3 } from './util';

import Modal from './components/Modal';
import Notes from './components/Notes';

const HEADER_HEIGHT = '30px';
const FOOTER_HEIGHT = '50px';

const Header = styled.header.withConfig({
  displayName: 'Header',
})`
  height: ${HEADER_HEIGHT};
  display: flex;
  flex-direction: row-reverse;

  & button {
    margin: 5px 10px 5px 0;
  }
`;

const Inner = styled.div.withConfig({
  displayName: 'Inner',
})`
  height: calc(100vh - ${HEADER_HEIGHT} - ${FOOTER_HEIGHT});
`;

const Footer = styled.div.withConfig({
  displayName: 'Footer',
})`
  height: ${FOOTER_HEIGHT};
  grid-area: save;  
  text-align: center;

  * {
    margin: 10px;
  }
`;

const WordCount = styled.div`
  position: relative;
  top: -1px;
  right: -1px;
  border: 1px solid gray;
  border-bottom-left-radius: 5px;
  padding: 5px;
  background-color: #dadada;
`;

const Container = styled.div.withConfig({
  displayName: 'EditorContainer',
})`
  padding: 20px;
  height: calc(100% - 40px);
  width: calc(100% - 40px);

  display: flex;

  & .ql-container {
    font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana, Helvetica, sans-serif;
    font-size: 14px;
    & p {
      margin: 1.286em auto;
    }
  }
`;

const FlexSpace = styled.div.withConfig({
  displayName: 'FlexSpace',
})`
  flex: 2;
`;

const FlexInner = styled.div.withConfig({
  displayName: 'FlexInner',
})`
  flex: 1 1 600px;
`;

const HiddenTextArea = styled.textarea.withConfig({
  displayName: 'HiddenTextArea',
})`
  position: absolute;
  left: -9999px;
`;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      wordCount: 0,
      work: '',
    };

    this.handleEdit = this.handleEdit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.work.entry === nextProps.work.entry) { return; }
    if (this.props.work.name !== nextProps.work.name) {
      document.title = `Writing Desk - ${nextProps.work.name}`;
    }
    this.handleEdit(nextProps.work.entry);
  }

  handleEdit(work) {
    const parsedWork = quillToWordcounter(work);
    const wordCount = (parsedWork.match(/(?!p\W|i\W|b\W)[a-zA-Z']*\w/g) || []).length;
    this.setState({
      wordCount,
      work,
    });
  }

  render() {
    const {
      modalIsActive,
      openExplorerModal,
      openMetaModal,
      openNotes,
      saveWork,
      saving,
      work,
    } = this.props;

    const handleCopy = () => {
      this.editor.select();
      document.execCommand('copy');
    };

    const handleConversion = () => {
      const parsedWork = AO3ToQuill(this.state.work);
      this.handleEdit(parsedWork);
    };

    const handleSave = () => {
      saveWork(work.id, { entry: this.state.work });
    };

    const renderSaveStatus = () => {
      if (saving) {
        return (
          <i className="fa fa-circle-o-notch fa-spin" />
        );
      }

      if (this.state.work === work.entry) {
        return (
          <i className="fa fa-check" />
        );
      }

      return undefined;
    };

    return (
      <div>
        { modalIsActive && <Modal />}
        <Notes />
        <HiddenTextArea
          value={quillToAO3(this.state.work)}
          innerRef={(el) => { this.editor = el; }}
        />
        <Header>
          <WordCount>{this.state.wordCount.toLocaleString()}</WordCount>
          <button type="button" onClick={handleCopy}>Copy for AO3</button>
          <button type="button" onClick={handleConversion}>Convert from AO3</button>
        </Header>
        <Inner>
          <Container>
            <FlexSpace />
            <FlexInner>
              <ReactQuill
                style={{ height: 'calc(100% - 40px)' }}
                onChange={this.handleEdit}
                value={this.state.work}
              />
            </FlexInner>
            <FlexSpace />
          </Container>
        </Inner>
        <Footer>
          {work.id ? (
            <React.Fragment>
              <button type="button" onClick={openNotes}>Notes</button>
              {work.name}
              <button type="button" onClick={() => { openMetaModal(work); }}>Edit</button>
              <button type="button" onClick={handleSave}>Save</button>
            </React.Fragment>
          ) : undefined}
          <button
            type="button"
            onClick={() => {
              openExplorerModal({ folderId: work.folderId || HOME.ID });
            }}
          >
            Load
          </button>
          {renderSaveStatus()}
        </Footer>
      </div>
    );
  }
}

App.propTypes = {
  modalIsActive: PropTypes.bool.isRequired,
  openExplorerModal: PropTypes.func.isRequired,
  openMetaModal: PropTypes.func.isRequired,
  openNotes: PropTypes.func.isRequired,
  saveWork: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
  work: PropTypes.shape({
    entry: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => ({
  modalIsActive: getModalActive(state),
  saving: getSaving(state),
  work: getWork(state),
});

const mapDispatchToProps = dispatch => ({
  openExplorerModal: (spec) => { dispatch(actions.openModal(MODALS.EXPLORER, spec)); },
  openMetaModal: (spec) => { dispatch(actions.openModal(MODALS.META, spec)); },
  openNotes: () => { dispatch(actions.openNotes()); },
  saveWork: (workId, spec) => { dispatch(actions.editWork(workId, spec)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
/* eslint-enable react/jsx-filename-extension */
