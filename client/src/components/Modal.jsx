import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../actions';
import { MODALS } from '../constants';
import { getModalType, getModalSpec } from '../reducers';

import ExplorerModal from './modals/ExplorerModal';
import MetaModal from './modals/MetaModal';
import MoveModal from './modals/MoveModal';

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;
`;

const Overlay = Container.extend`
  background-color: #000;
  opacity: 0.2;
  z-index: -1;
`;

const Window = styled.div`
  background-color: #FFFFFF;
  border-radius: 0 0 2px 2px;
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.4);
  color: #616161;
  margin: 15vh auto;
  padding: 40px;
  position: relative;
  width: 500px;
  z-index: 1;
`;

const Modal = ({ closeModal, spec, type }) => {
  const renderTypeModal = () => {
    switch (type) {
      case MODALS.EXPLORER:
        return <ExplorerModal {...spec} />;
      case MODALS.META:
        return <MetaModal {...spec} />;
      case MODALS.MOVE:
        return <MoveModal {...spec} />;
      default:
        return <div />;
    }
  };

  return (
    <Container>
      <Window>
        {renderTypeModal()}
      </Window>
      <Overlay onClick={closeModal} />
    </Container>
  );
};

Modal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  spec: PropTypes.shape({}).isRequired,
  type: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  spec: getModalSpec(state),
  type: getModalType(state),
});

const mapDispatchToProps = dispatch => ({
  closeModal: () => { dispatch(actions.closeModal()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
