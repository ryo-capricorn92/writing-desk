import PropTypes from 'prop-types';
import React from 'react';
import ReactQuill from 'react-quill';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../actions';
import { getNotes, getWork } from '../reducers';

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: calc(50% - 45px);
  pointer-events: none;
  z-index: 100;
`;

const Flyover = styled.div`
  position: relative;
  top: 0;
  left: ${({ active }) => (active ? '0' : 'calc(-100% - 40px)')};
  transition: left 1s ease;
  height: 100%;
  width: 100%;
  pointer-events: initial;

  background-color: #fff;
  box-shadow: 0 0 5px 5px #999;
  padding: 20px;

  & > * {
    margin-bottom: 20px;
  }

  & > center {
    margin-top: 60px;
  }
`;

const Header = styled.header`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const CloseButton = styled.div`
  cursor: pointer;
  color: #666;
`;

class Notes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: props.work.notes || '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (this.props.active !== newProps.active) {
      this.setState({
        notes: newProps.work.notes || '',
      });
    }
  }

  handleChange(notes) {
    this.setState({ notes });
  }

  render() {
    const { active, close, saveWork, work } = this.props; // eslint-disable-line object-curly-newline, max-len
    const handleSave = () => {
      saveWork(work.id, { notes: this.state.notes });
    };

    return (
      <Container>
        <Flyover {...{ active }}>
          <Header>
            <span>Notes</span>
            <CloseButton onClick={() => { close(); }}>
              <i className="fa fa-close fa-2x" />
            </CloseButton>
          </Header>
          <ReactQuill
            style={{ height: '80%' }}
            onChange={this.handleChange}
            value={this.state.notes}
          />
          <center>
            <button type="button" onClick={handleSave}>Save</button>
          </center>
        </Flyover>
      </Container>
    );
  }
}

Notes.propTypes = {
  active: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  saveWork: PropTypes.func.isRequired,
  work: PropTypes.shape({
    id: PropTypes.string,
    notes: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => ({
  active: getNotes(state),
  work: getWork(state),
});

const mapDispatchToProps = dispatch => ({
  close: () => { dispatch(actions.closeNotes()); },
  // saveWork: (chapterId, spec) => { dispatch(actions.editChapter(chapterId, spec)); },
  saveWork: (id, spec) => { dispatch(actions.editWork(id, spec)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Notes);
