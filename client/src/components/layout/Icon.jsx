import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const Inner = styled.i.attrs({
  className: ({
    fixed, size, spin, type,
  }) => [
    'fa',
    `fa-${type}`,
    (fixed ? 'fa-fw' : ''),
    (size ? `fa-${size}` : ''),
    (spin ? 'fa-spin' : ''),
  ].join(' '),
}).withConfig({
  displayName: 'Inner',
})`
  background: transparent;
  ${({ color }) => (color ? `color: ${color}` : '')}
  ${({ tweaks }) => tweaks}
`;

const Icon = ({ style, ...props }) => <Inner {...props} tweaks={style} />;

Icon.defaultProps = {
  color: null,
  fixed: false,
  size: null,
  spin: false,
  style: null,
  title: null,
};

Icon.propTypes = {
  color: PropTypes.string,
  fixed: PropTypes.bool,
  size: PropTypes.string,
  spin: PropTypes.bool,
  style: PropTypes.shape({}),
  title: PropTypes.string,
  type: PropTypes.string.isRequired,
};

export default Icon;
