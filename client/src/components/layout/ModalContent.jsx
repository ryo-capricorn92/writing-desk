import styled from 'styled-components';

const ModalContet = styled.div.withConfig({
  displayName: 'ModalContent',
})`
  height: ${({ extended }) => (extended ? 'calc(50vh + 60px)' : '50vh')};
  overflow-y: auto;
`;

export default ModalContet;
