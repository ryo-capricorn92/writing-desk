import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

import Icon from '../layout/Icon';

const Container = styled.div.withConfig({
  displayName: 'AccordianItem_Container',
})`
  align-items: center;
  display: flex;

  & > i {
    flex: 0;
    margin: 5px;
  }
`;

const Name = styled.div.withConfig({
  displayName: 'AccordianItem_Name',
})`
  border-radius: 5px;
  padding: 5px;

  ${({ isSelected }) => isSelected && `
    background-color: #36D7B7;
    color: #FFF;
  `}
`;

const Children = styled.div.withConfig({
  displayName: 'AccordianItem_Children',
})`
  margin-left: 20px;
  ${({ isExtended }) => (isExtended ? '' : 'display: none;')}
`;

const AccordianItem = (props) => {
  const { children, extended, id, name, selected } = props;
  const { select, toggleExtended } = props;

  const isExtended = extended.includes(id);
  const isSelected = selected === id;

  const checkForExtended = c => c.id === selected || c.children.some(checkForExtended);
  const hasSelectedChild = children.some(checkForExtended);

  return (
    <div>
      <Container>
        <Icon
          type={isExtended ? 'caret-down' : 'caret-right'}
          onClick={() => { toggleExtended(id, !isExtended); }}
          color={hasSelectedChild ? '#36D7B7' : ''}
        />
        <Name onClick={() => { select(id); }} {...{ isSelected }}>{name}</Name>
      </Container>
      <Children {...{ isExtended }}>
        {children.map(child => (
          <AccordianItem
            {...child}
            {...{ extended, select, selected, toggleExtended }}
            key={child.id}
          />
        ))}
      </Children>
    </div>
  );
};

AccordianItem.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  extended: PropTypes.arrayOf(PropTypes.string).isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  select: PropTypes.func.isRequired,
  selected: PropTypes.string.isRequired,
  toggleExtended: PropTypes.func.isRequired,
};

export default AccordianItem;
