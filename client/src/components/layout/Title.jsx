import styled from 'styled-components';

const Title = styled.h2.withConfig({
  displayName: 'Title',
})`
  font-size: 16px;
  text-align: center;
`;

export default Title;
