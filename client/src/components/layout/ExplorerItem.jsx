import { setLightness } from 'polished';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../../actions';
import { EXPLORER_ACTIONS } from '../../constants';

import Icon from './Icon';

const Container = styled.div.withConfig({
  displayName: 'ExplorerItem_Container',
})`
  align-items: center;
  border: 1px solid #b2b2b2;
  border-radius: ${({ isFolder }) => (isFolder ? '5px' : '25px')}; 
  color: #666;
  display: flex;
  margin: 5px;
  overflow: hidden;
`;

const Item = styled.div.withConfig({
  displayName: 'ExplorerItem_Item',
})`
  background-color: #ededed;
  flex: 0 0 20px;
  padding: ${({ fat }) => (fat ? '11px 7px' : '7px')};
  text-align: center;

  & + & {
    border-left: 1px solid #bfbfbf;
  }

  ${({ onClick }) => (onClick ? `
    cursor: pointer;

    &:hover {
      filter: brightness(95%);
    }
  ` : '')}

  ${({ color }) => (color ? `
    color: ${color};
    background-color: ${setLightness(0.92, color)};
  ` : '')}
`;

const Title = Item.extend.withConfig({
  displayName: 'ExplorerItem_Title',
})`
  background-color: #fafafa;
  border-left: 1px solid #bfbfbf;
  border-right: 1px solid #bfbfbf;
  display: flex;
  flex: auto;
  flex-direction: column;
  text-align: left;
`;

const SubTitle = styled.div.withConfig({
  displayName: 'ExplorerItem_SubTitle',
})`
  font-size: 9px;
`;

const Edit = Item.extend.withConfig({
  displayName: 'ExplorerItem_Edit',
})`
  background-color: #fff;
  display: flex;
  flex: 1;
  padding: 0 7px;
`;

const Textbox = styled.input.withConfig({
  displayName: 'ExplorerItem_Textbox',
}).attrs({
  type: 'text',
})`
  border: none;
  border-bottom: 1px solid #ddd;
  flex: 2;
  width: 100%;

  & + & {
    flex: 1;
    margin-left: 5px;
  }

  &:focus {
    outline: none;
    border-bottom: 1px solid #666;
  }
`;

const Warning = Item.extend.withConfig({
  displayName: 'ExplorerItem_Warning',
})`
  background-color: #f7f1ea;
  border-right: 1px solid #bfbfbf;
  color: #e58e26;
  flex: 1;
  text-align: center;
`;

class ExplorerItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
      isWarning: false,
      spec: {
        name: props.title,
        icon: props.icon,
        color: props.color,
      },
      warningAction: () => {},
      warningMessage: '',
    };

    this.openEdit = this.openEdit.bind(this);
    this.openWarning = this.openWarning.bind(this);
    this.closeEdit = this.closeEdit.bind(this);
    this.closeWarning = this.closeWarning.bind(this);
    this.updateSpec = this.updateSpec.bind(this);
  }

  openEdit() {
    this.setState({
      isEditing: true,
    });
  }

  openWarning(warningMessage, warningAction) {
    this.setState({
      isWarning: true,
      warningAction,
      warningMessage,
    });
  }

  closeEdit() {
    this.setState({
      isEditing: false,
    });
  }

  closeWarning() {
    this.setState({
      isWarning: false,
      warningAction: () => {},
      warningMessage: '',
    });
  }

  updateSpec(key) {
    return (e) => {
      const spec = {
        ...this.state.spec,
        [key]: e.target.value,
      };
      this.setState({ spec });
    };
  }

  render() {
    const { isEditing, isWarning, warningAction, warningMessage } = this.state;
    const { color, folderId, icon, id, isFolder, name } = this.props;
    const { onClick, options, subtitle, title } = this.props;
    const defaultIcon = isFolder ? 'folder' : 'file-text-o';

    const actionSpec = { folderId, id, isFolder, name };

    const handleOptionAction = option => () => {
      switch (option.action) {
        case EXPLORER_ACTIONS.EDIT:
          this.openEdit();
          break;
        default:
          if (option.warning) {
            this.openWarning(option.warning, option.action);
          } else {
            option.action(actionSpec);
          }
      }
    };

    const handleEdit = () => {
      if (isFolder) {
        this.props.editFolder(id, this.state.spec);
      } else {
        this.props.editWork(id, this.state.spec);
      }
      this.closeEdit();
    };

    if (isWarning) {
      return (
        <Container {...{ isFolder }}>
          <Warning>{warningMessage}</Warning>
          <Item onClick={() => { warningAction(actionSpec); }}>
            <Icon type="check" />
          </Item>
          <Item onClick={this.closeWarning}>
            <Icon type="close" />
          </Item>
        </Container>
      );
    }

    if (isEditing) {
      return (
        <Container {...{ isFolder }}>
          <Edit>
            <Textbox
              placeholder="title"
              onChange={this.updateSpec('name')}
              value={this.state.spec.name}
            />
            <Textbox
              placeholder="icon"
              onChange={this.updateSpec('icon')}
              value={this.state.spec.icon}
            />
            <Textbox
              placeholder="color"
              onChange={this.updateSpec('color')}
              value={this.state.spec.color}
            />
          </Edit>
          <Item onClick={handleEdit}>
            <Icon type="check" />
          </Item>
          <Item onClick={this.closeEdit}>
            <Icon type="close" />
          </Item>
        </Container>
      );
    }

    return (
      <Container {...{ isFolder }}>
        <Item {...{ color }} fat={subtitle}>
          <Icon type={icon || defaultIcon} />
        </Item>
        <Title {...{ onClick }} style={{ fontSize: subtitle ? '12px' : '14px' }}>
          <span>{title}</span>
          {subtitle && (<SubTitle>{subtitle}</SubTitle>)}
        </Title>
        {options.map(option => (
          <Item onClick={handleOptionAction(option)} key={option.name} fat={subtitle}>
            <Icon
              type={option.icon}
              alt={option.name}
            />
          </Item>
        ))}
      </Container>
    );
  }
}

ExplorerItem.propTypes = {
  color: PropTypes.string,
  editFolder: PropTypes.func.isRequired,
  editWork: PropTypes.func.isRequired,
  folderId: PropTypes.string.isRequired,
  icon: PropTypes.string,
  id: PropTypes.string.isRequired,
  isFolder: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    action: PropTypes.oneOfType([PropTypes.func, PropTypes.string]).isRequired,
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })),
  subtitle: PropTypes.string,
  title: PropTypes.string.isRequired,
};

ExplorerItem.defaultProps = {
  color: '#666',
  icon: null,
  isFolder: false,
  options: [],
  subtitle: '',
};

const mapDispatchToProps = dispatch => ({
  editFolder: (id, spec) => { dispatch(actions.editFolder(id, spec)); },
  editWork: (id, spec) => { dispatch(actions.editWorkQuick(id, spec)); },
});

export default connect(null, mapDispatchToProps)(ExplorerItem);
