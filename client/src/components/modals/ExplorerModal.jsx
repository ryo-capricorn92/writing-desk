import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import * as ReactTabs from 'react-tabs';
import styled from 'styled-components';

import * as actions from '../../actions';
import { HOME } from '../../constants';
import { getFolder, getLoading, getModalSpec, getDrafts, getSelectionTab } from '../../reducers';

import ExplorerTab from './ExplorerTab';
import DraftsTab from './DraftsTab';

import Icon from '../layout/Icon';
import ModalContent from '../layout/ModalContent';
import Title from '../layout/Title';

const Center = styled.div.withConfig({
  displayName: 'ExplorerModal_Center',
})`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const Tabs = styled(ReactTabs.Tabs).withConfig({
  displayName: 'ExplorerModal_Tabs',
})``;

const TabList = styled(ReactTabs.TabList).withConfig({
  displayName: 'ExplorerModal_TabList',
})`
  display: flex;
  list-style-type: none;
  margin: 0;
  padding: 0;
  position: absolute;
  top: 0;
  left: 40px;
`;

const Tab = styled(ReactTabs.Tab).withConfig({
  displayName: 'ExplorerModal_Tab',
})`
  border: 1px solid #666;
  border-top: none;
  border-radius: 0 0 5px 5px;
  cursor: pointer;
  padding: 5px;

  &+& {
    margin-left: 1px;
  }

  ${({ selected }) => (selected ? `
    background-color: #666;
    border-color: #666;
    color: #FFF;
    margin: 0 1px;
  ` : `
    &:hover {
      background-color: #BBB;
      border-color: #BBB;
      color: #FFF;
      margin: 0 1px;
    }
  `)}
`;

const TabPanel = styled(ReactTabs.TabPanel).withConfig({
  displayName: 'ExplorerModal_TabPanel',
})``;

class ExplorerModal extends React.Component {
  constructor(props) {
    super(props);

    if (!props.folder.id) {
      props.goToFolder(props.folderId || HOME.ID);
    }

    if (!props.drafts) {
      props.getDrafts();
    }
  }

  render() {
    const { drafts, folder, loading, tab, selectTab } = this.props;

    if (loading || !folder.id || !drafts) {
      return (
        <div>
          <Title>loading . . .</Title>
          <ModalContent>
            <Center>
              <Icon type="circle-o-notch" size="2x" spin />
            </Center>
          </ModalContent>
        </div>
      );
    }

    const wips = drafts.filter(d => d.active);
    const backlog = drafts.filter(d => !d.active);

    return (
      <div>
        <Tabs
          selectedIndex={tab}
          onSelect={selectTab}
        >
          <TabList>
            <Tab>Explorer</Tab>
            <Tab>WIPs</Tab>
            <Tab>Backlog</Tab>
          </TabList>
          <TabPanel>
            <ExplorerTab {...{ folder }} />
          </TabPanel>
          <TabPanel>
            <DraftsTab
              works={wips}
            />
          </TabPanel>
          <TabPanel>
            <DraftsTab
              works={backlog}
              backlog
            />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

ExplorerModal.defaultProps = {
  drafts: null,
};

ExplorerModal.propTypes = {
  drafts: PropTypes.arrayOf(PropTypes.shape({})),
  getDrafts: PropTypes.func.isRequired,
  goToFolder: PropTypes.func.isRequired,
  folder: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  folderId: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  selectTab: PropTypes.func.isRequired,
  tab: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  drafts: getDrafts(state),
  folder: getFolder(state),
  loading: getLoading(state),
  spec: getModalSpec(state),
  tab: getSelectionTab(state),
});

const mapDispatchToProps = dispatch => ({
  getDrafts: () => { dispatch(actions.getDrafts()); },
  goToFolder: (folderId) => { dispatch(actions.getFolder(folderId)); },
  selectTab: (tab) => { dispatch(actions.selectTab(tab)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ExplorerModal);
