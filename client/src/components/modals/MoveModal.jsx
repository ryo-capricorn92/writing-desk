import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../../actions';
import { MODALS } from '../../constants';
import { getFolder, getPedigree, getLoading } from '../../reducers';
import { sortChildren } from '../../util';

import AccordianItem from '../layout/AccordianItem';
import Icon from '../layout/Icon';

const Content = styled.div.withConfig({
  displayName: 'MoveModal_Content',
})`
  height: 55vh;
  overflow-y: auto;
`;

const Center = styled.div.withConfig({
  displayName: 'MoveModal_Center',
})`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const Title = styled.h2.withConfig({
  displayName: 'MoveModal_Title',
})`
  font-size: 16px;
  text-align: center;
`;

const Buttons = styled.div.withConfig({
  displayName: 'MoveModal_Buttons',
})`
  margin: 5px;
  text-align: center;

  button {
    margin: 5px;
  }
`;

class MoveModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      extended: [],
      selected: props.originalParentId || '',
    };

    this.select = this.select.bind(this);
    this.toggleExtended = this.toggleExtended.bind(this);

    props.getPedigree();
  }

  select(id) {
    this.setState({
      selected: id,
    });
  }

  toggleExtended(id, extend) {
    if (extend) {
      this.setState({
        extended: [...this.state.extended, id],
      });
    } else {
      this.setState({
        extended: this.state.extended.filter(i => i !== id),
      });
    }
  }

  render() {
    const { loading, id, isFolder, name, originalParentId } = this.props;
    const { backToExplorer, moveChild } = this.props;

    const filterChildren = (child) => {
      if (!child.children) { return child; }
      const filtered = child.children
        .filter(c => c.id !== id)
        .sort(sortChildren)
        .map(filterChildren);

      return {
        ...child,
        children: filtered,
      };
    };

    const pedigree = filterChildren(this.props.pedigree);

    const handleMoveChild = () => {
      moveChild(id, this.state.selected || originalParentId, isFolder);
    };

    if (loading || !pedigree.id) {
      return (
        <div>
          <Title>loading . . .</Title>
          <Content>
            <Center>
              <Icon type="circle-o-notch" size="2x" spin />
            </Center>
          </Content>
        </div>
      );
    }

    return (
      <div>
        <Title>{`Move "${name}"`}</Title>
        <Content>
          <AccordianItem
            {...pedigree}
            extended={this.state.extended}
            selected={this.state.selected}
            select={this.select}
            toggleExtended={this.toggleExtended}
          />
        </Content>
        <Buttons>
          <button
            type="button"
            onClick={() => { backToExplorer(originalParentId); }}
          >
            Back
          </button>
          <button
            type="button"
            onClick={handleMoveChild}
          >
            Move {isFolder ? 'Folder' : 'Work'}
          </button>
        </Buttons>
      </div>
    );
  }
}

MoveModal.defaultProps = {
  name: '',
};

MoveModal.propTypes = {
  backToExplorer: PropTypes.func.isRequired,
  getPedigree: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  isFolder: PropTypes.bool.isRequired,
  moveChild: PropTypes.func.isRequired,
  name: PropTypes.string,
  originalParentId: PropTypes.string.isRequired,
  pedigree: PropTypes.shape({
    children: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => ({
  loading: getLoading(state),
  originalParentId: getFolder(state).id || '',
  pedigree: getPedigree(state),
});

const mapDispatchToProps = dispatch => ({
  backToExplorer: (id) => { dispatch(actions.openModal(MODALS.EXPLORER, { folderId: id })); },
  getPedigree: () => { dispatch(actions.getFolderPedigree()); },
  moveChild: (id, parentId, isFolder) => { dispatch(actions.moveChild(id, parentId, isFolder)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(MoveModal);
