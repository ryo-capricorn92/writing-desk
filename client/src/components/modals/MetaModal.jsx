import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../../actions';

import Icon from '../layout/Icon';

const Flex = styled.div.withConfig({
  displayName: 'MetaModal_Flex',
})`
  align-items: flex-end;
  display: flex;
  margin: 10px 0;
  width: 100%;
`;

const Buttons = styled.div.withConfig({
  displayName: 'MetaModal_Buttons',
})`
  display: block;
  margin: 10px 0;
  text-align: center;
`;

const WithLabel = styled.div.withConfig({
  displayName: 'MetaModal_WithLabel',
})`
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  padding-right: 15px;

  div {
    display: flex;

    span, i {
      margin-left: 5px;
    }
  }
`;

const Textbox = styled.input.withConfig({
  displayName: 'MetaModal_Textbox',
}).attrs({
  type: 'text',
})`
  border: 1px solid #666;
  border-radius: 3px;
  margin: 5px 0;
  padding: 5px;
`;

const Option = styled.div.withConfig({
  displayName: 'MetaModal_Option',
})`
  flex: 0 60px;
  padding-bottom: 12px;

  input[type="checkbox"] {
    margin-right: 5px;
  }
`;

const ColorBlock = styled.span.withConfig({
  displayName: 'MetaModal_ColorBlock',
})`
  width: 15px;
  height: 15px;
  border-radius: 6px;

  ${({ color }) => (color ? `background-color: ${color}` : '')}
`;

class MetaModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      active: props.active,
      color: props.color,
      draft: props.draft,
      icon: props.icon,
      name: props.name,
    };
  }

  updateSpec(spec) {
    this.setState({
      ...spec,
    });
  }

  render() {
    const { active, color, draft, icon, name } = this.state;
    const { closeModal, editWork } = this.props;

    const apply = (key, isBool = false) => (e) => {
      const val = isBool ? !this.state[key] : e.target.value;
      const spec = {
        [key]: val,
      };
      this.updateSpec(spec);
    };

    const handleSave = () => {
      const spec = { ...this.state };
      editWork(this.props.id, spec);
    };

    return (
      <div>
        <Flex>
          <WithLabel>
            Name
            <Textbox value={name} onChange={apply('name')} />
          </WithLabel>
          <Option>
            <input type="checkbox" checked={draft} onChange={apply('draft', true)} />
            <span>Draft</span>
          </Option>
        </Flex>
        <Flex>
          <WithLabel>
            <div>
              Icon <Icon type={icon} />
            </div>
            <Textbox value={icon} onChange={apply('icon')} />
          </WithLabel>
          <WithLabel>
            <div>
              Color <ColorBlock {...{ color }} />
            </div>
            <Textbox value={color} onChange={apply('color')} />
          </WithLabel>
          <Option>
            <input type="checkbox" checked={active} onChange={apply('active', true)} />
            <span>Active</span>
          </Option>
        </Flex>
        <Buttons>
          <button
            type="button"
            onClick={() => { closeModal(); }}
          >
            Cancel
          </button>
          <button
            type="button"
            onClick={handleSave}
          >
            Save
          </button>
        </Buttons>
      </div>
    );
  }
}

MetaModal.propTypes = {
  active: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  draft: PropTypes.bool.isRequired,
  editWork: PropTypes.func.isRequired,
  icon: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

const mapDispatchToProps = dispatch => ({
  closeModal: () => { dispatch(actions.closeModal()); },
  editWork: (id, spec) => { dispatch(actions.editWorkMeta(id, spec)); },
});

export default connect(null, mapDispatchToProps)(MetaModal);
