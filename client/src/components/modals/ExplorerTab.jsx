import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../../actions';
import { EXPLORER_ACTIONS, MODALS } from '../../constants';
import { sortChildren } from '../../util';

import ExplorerItem from '../layout/ExplorerItem';
import ModalContent from '../layout/ModalContent';
import Title from '../layout/Title';

const Nav = styled.nav.withConfig({
  displayName: 'ExplorerModal_Nav',
})`
  font-size: 13px;
  margin-bottom: 5px;
`;

const NavItem = styled.span.withConfig({
  displayName: 'ExplorerModal_NavItem',
})`
  display: inline-block;

  ${({ onClick }) => (
    onClick ? `
      cursor: pointer;
      font-weight: bold;

      &:hover {
        font-weight: normal;
      }
    ` : `
      &::before {
        content: "»";
        padding: 0 5px;
      }
    `
  )}

  &+&::before {
    content: "»";
    font-weight: normal;
    padding: 0 5px;
  }
`;

const Buttons = styled.div.withConfig({
  displayName: 'ExplorerModal_Buttons',
})`
  margin: 5px;
  text-align: center;

  button {
    margin: 5px;
  }
`;

const ExplorerTab = (props) => {
  const { folder, goToFolder, openMoveModal, openWork } = props;
  const { addFolder, addWork, deleteFolder, deleteWork } = props;

  const handleClick = ({ id, isFolder }) => {
    if (isFolder) {
      goToFolder(id);
    } else {
      openWork(id);
    }
  };

  const handleMove = ({ id, isFolder, name }) => {
    openMoveModal({ id, isFolder, name });
  };

  const handleDelete = ({ id, isFolder }) => {
    if (isFolder) {
      deleteFolder(id);
    } else {
      deleteWork(id);
    }
  };

  const sortedChildren = folder.children.sort(sortChildren);

  const options = [
    {
      action: EXPLORER_ACTIONS.EDIT,
      icon: 'pencil',
      name: 'edit',
    },
    {
      action: handleMove,
      icon: 'share',
      name: 'move',
    },
    {
      action: handleDelete,
      icon: 'trash',
      name: 'delete',
      warning: 'Are you sure you would like to delete this?',
    },
  ];

  return (
    <div>
      <Title>{folder.name}</Title>
      <Nav>
        {folder.ancestry.map(crumb => (
          <NavItem
            onClick={folder.id === crumb.id ? null : () => { goToFolder(crumb.id); }}
            key={crumb.id}
          >
            {crumb.name}
          </NavItem>
        ))}
      </Nav>
      <ModalContent>
        {sortedChildren.map(child => (
          <ExplorerItem
            {...child}
            title={child.name}
            onClick={() => { handleClick(child); }}
            key={child.id}
            {...{ options }}
          />
          ))}
      </ModalContent>
      <Buttons>
        <button
          type="button"
          onClick={() => { addFolder(folder.id); }}
        >
          New Folder
        </button>
        <button
          type="button"
          onClick={() => { addWork(folder.id); }}
        >
          New Work
        </button>
      </Buttons>
    </div>
  );
};

ExplorerTab.propTypes = {
  addFolder: PropTypes.func.isRequired,
  addWork: PropTypes.func.isRequired,
  folder: PropTypes.shape({
    ancestry: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })),
    children: PropTypes.arrayOf(PropTypes.shape({
      color: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      isFolder: PropTypes.bool.isRequired,
      name: PropTypes.string.isRequired,
    })),
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  deleteFolder: PropTypes.func.isRequired,
  deleteWork: PropTypes.func.isRequired,
  goToFolder: PropTypes.func.isRequired,
  openMoveModal: PropTypes.func.isRequired,
  openWork: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  addFolder: (parentId) => { dispatch(actions.addFolder(parentId)); },
  addWork: (parentId) => { dispatch(actions.addWork(parentId)); },
  deleteFolder: (folderId) => { dispatch(actions.deleteFolder(folderId)); },
  deleteWork: (workId) => { dispatch(actions.deleteWork(workId)); },
  goToFolder: (folderId) => { dispatch(actions.getFolder(folderId)); },
  openMoveModal: (spec) => { dispatch(actions.openModal(MODALS.MOVE, spec)); },
  openWork: (id) => { dispatch(actions.getWork(id)); },
});

export default connect(null, mapDispatchToProps)(ExplorerTab);
