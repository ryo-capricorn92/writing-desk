import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import ExplorerItem from '../layout/ExplorerItem';
import ModalContent from '../layout/ModalContent';
import Title from '../layout/Title';

const DraftsTab = ({ backlog, goToFolder, moveToOppList, openWork, works }) => {
  const options = [
    {
      action: ({ folderId }) => { goToFolder(folderId); },
      icon: 'folder',
      name: 'go-to-folder',
    },
    {
      action: ({ id }) => { moveToOppList(id, backlog); },
      icon: 'share',
      name: 'move-to-backlog',
    },
  ];

  const sortedWorks = works.sort((a, b) => {
    if (a.timestamp > b.timestamp) { return -1; }
    if (b.timestamp > a.timestamp) { return 1; }
    return 0;
  });

  return (
    <div>
      <Title>{backlog ? 'Backlog' : 'WIPs'}</Title>
      <ModalContent extended>
        {sortedWorks.map(work => (
          <ExplorerItem
            {...work}
            title={work.name}
            onClick={() => { openWork(work.id); }}
            key={work.id}
            {...{ options }}
          />
        ))}
      </ModalContent>
    </div>
  );
};

DraftsTab.defaultProps = {
  backlog: false,
  works: [],
};

DraftsTab.propTypes = {
  backlog: PropTypes.bool,
  goToFolder: PropTypes.func.isRequired,
  moveToOppList: PropTypes.func.isRequired,
  openWork: PropTypes.func.isRequired,
  works: PropTypes.arrayOf(PropTypes.shape({
    color: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })),
};

const mapDispatchToProps = dispatch => ({
  goToFolder: (folderId) => { dispatch(actions.getFolder(folderId)); },
  moveToOppList: (id, active) => { dispatch(actions.updateActiveStatus(id, active)); },
  openWork: (id) => { dispatch(actions.getWork(id)); },
});

export default connect(null, mapDispatchToProps)(DraftsTab);
