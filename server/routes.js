const { Folder, Work } = require('./util');

const getAllFolders = (req, res) => {
  Folder.findAll().then((folders) => { res.json({ folders }); });
};

const getAllWorks = (req, res) => {
  Work.findAll().then((works) => { res.json({ works }); });
};

const getDrafts = (req, res) => {
  Work.getDraftsWithInheritence().then((drafts) => { res.json({ drafts }); });
};

const getFolderPedigree = (req, res) => {
  Folder.getAllFolderPedigree().then((pedigree) => { res.json({ pedigree }); });
};

const getFolder = (req, res) => {
  Folder.getFolderWithChildren(req.params.id)
    .then((folder) => {
      res.json({
        folder,
      });
    });
};

const getWork = (req, res) => {
  Work.findByPk(req.params.id, { raw: true }).then((work) => { res.json({ work }); });
};

const addNewFolder = (req, res) => {
  const { folder = {}, parentId } = req.body;

  Folder.newFolder(folder, parentId)
    .then((newFolder) => {
      Folder.getFolderWithChildren(parentId)
        .then((parent) => {
          res.json({
            folder: newFolder.dataValues,
            parent,
          });
        });
    });
};

const addNewWork = (req, res) => {
  const { work = {}, parentId } = req.body;

  Work.newWork(work, parentId)
    .then((newWork) => {
      Folder.getFolderWithChildren(parentId)
        .then((parent) => {
          res.json({
            parent,
            work: newWork.dataValues,
          });
        });
    });
};

const updateFolder = (req, res) => {
  const { id, spec = {} } = req.body;

  Folder.findByPk(id)
    .then((folder) => {
      const validOnly = {};
      Object.keys(spec).forEach((key) => {
        if (spec[key] === undefined) { return; }

        validOnly[key] = spec[key];
      });

      folder.update(validOnly, { fields: ['color', 'icon', 'name'] })
        .then((updated) => {
          Folder.getFolderWithChildren(updated.folderId)
            .then((parent) => {
              res.json({
                folder: updated,
                parent,
              });
            });
        });
    });
};

const updateWork = (req, res) => {
  const { id, spec = {} } = req.body;

  Work.findByPk(id)
    .then((found) => {
      const validOnly = {};
      Object.keys(spec).forEach((key) => {
        if (spec[key] === undefined) { return; }

        validOnly[key] = spec[key];
      });

      // set work to active WIP when entry is updated
      if (validOnly.entry && validOnly.entry !== found.entry) {
        validOnly.active = true;
      }

      found.update(validOnly, { fields: ['active', 'color', 'draft', 'entry', 'icon', 'name', 'notes', 'number'] })
        .then(async (updated) => {
          let drafts;
          let parent;
          let work;
          const toReturn = req.body.toReturn || ['drafts', 'parent', 'drafts'];

          if (toReturn.includes('drafts')) {
            drafts = await Work.getDraftsWithInheritence();
          }

          if (toReturn.includes('parent')) {
            parent = await Folder.getFolderWithChildren(updated.folderId);
          }

          if (toReturn.includes('work')) {
            work = updated;
          }

          res.json({ drafts, parent, work });
        });
    });
};

const moveFolder = (req, res) => {
  Folder.moveFolder(req.body.id, req.body.newParentId)
    .then((newParentFolder) => {
      res.json({
        newParentFolder,
      });
    });
};

const moveWork = (req, res) => {
  Work.moveWork(req.body.id, req.body.newParentId)
    .then((newParentFolder) => {
      res.json({
        newParentFolder,
      });
    });
};

const deleteFolder = (req, res) => {
  Folder.findByPk(req.params.id)
    .then((folder) => {
      const parentId = folder.folderId;
      folder.destroy()
        .then(() => {
          Folder.getFolderWithChildren(parentId)
            .then((parent) => {
              res.json({ parent });
            });
        });
    });
};

const deleteWork = (req, res) => {
  Work.findByPk(req.params.id)
    .then((work) => {
      const parentId = work.folderId;
      work.destroy()
        .then(() => {
          Folder.getFolderWithChildren(parentId)
            .then((parent) => {
              res.json({ parent });
            });
        });
    });
};

module.exports = (app) => {
  app.get('/api/folders', getAllFolders);
  app.get('/api/works', getAllWorks);

  app.get('/api/folders/pedigree', getFolderPedigree);
  app.get('/api/works/drafts', getDrafts);

  app.get('/api/folder/:id', getFolder);
  app.get('/api/work/:id', getWork);

  app.post('/api/new/folder', addNewFolder);
  app.post('/api/new/work', addNewWork);

  app.put('/api/update/folder', updateFolder);
  app.put('/api/update/work', updateWork);

  app.put('/api/move/folder', moveFolder);
  app.put('/api/move/work', moveWork);

  app.delete('/api/delete/folder/:id', deleteFolder);
  app.delete('/api/delete/work/:id', deleteWork);
};
