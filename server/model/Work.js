const Sequelize = require('sequelize');
const db = require('../db');

const Work = db.define('work', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  active: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  color: {
    type: Sequelize.STRING,
    defaultValue: '',
  },
  draft: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  entry: {
    type: Sequelize.TEXT,
    defaultValue: '',
  },
  icon: {
    type: Sequelize.STRING,
    defaultValue: '',
  },
  name: {
    type: Sequelize.STRING,
    defaultValue: 'New Work',
  },
  notes: {
    type: Sequelize.TEXT,
    defaultValue: '',
  },
  number: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
  },
});

module.exports = Work;
