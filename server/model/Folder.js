const Sequelize = require('sequelize');
const db = require('../db');

const Folder = db.define('folder', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  color: {
    type: Sequelize.STRING,
    defaultValue: '',
  },
  icon: {
    type: Sequelize.STRING,
    defaultValue: '',
  },
  ignoreDrafts: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  name: {
    type: Sequelize.STRING,
    defaultValue: 'New Folder',
  },
});

module.exports = Folder;
