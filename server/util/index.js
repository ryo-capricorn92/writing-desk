const FolderModel = require('../model/Folder');
const WorkModel = require('../model/Work');

FolderModel.hasMany(WorkModel);
WorkModel.belongsTo(FolderModel);

FolderModel.hasMany(FolderModel);
FolderModel.belongsTo(FolderModel);

const Folder = require('./Folder');
const Work = require('./Work');

module.exports = { Folder, Work };
