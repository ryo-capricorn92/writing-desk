const { Op } = require('sequelize');

const Folder = require('../model/Folder');
const Work = require('../model/Work');

const { HOME } = require('../constants');

const NON_EMPTY_DRAFTS = {
  draft: true,
  entry: {
    [Op.notRegexp]: '^(<p>(\\s*|<br>)</p>)$',
    [Op.not]: '',
  },
};

Work.getDraftsWithInheritence = () => Work.findAll({ where: NON_EMPTY_DRAFTS })
  .then(works => works.reduce(async (memo, work) => {
    const arr = await memo;
    const spec = {
      active: work.active,
      color: work.color,
      folderId: work.folderId,
      icon: work.icon,
      id: work.id,
      name: work.name,
      timestamp: work.updatedAt,
    };

    if (!work.color && !work.icon && work.folderId) {
      const { color, icon } = await Folder.getClosestIcon(work.folderId);
      spec.color = color;
      spec.icon = icon;
    }

    arr.push(spec);

    return arr;
  }, Promise.resolve([])));

Work.newWork = (work, parentId) => Work.create(work)
  .then((newWork) => {
    if (!parentId || parentId === HOME.ID) {
      return newWork;
    }

    return Folder.findByPk(parentId)
      .then((parent) => {
        if (!parent) {
          return newWork;
        }

        return parent.addWork(newWork)
          .then(() => ({ ...newWork, folderId: parent.id }));
      });
  });

Work.moveWork = (workId, newParentId) => Work.findByPk(workId)
  .then((work) => {
    // if in HOME folder, no need to remove from current
    if (!work.folderId) {
      if (!newParentId || newParentId === HOME.ID) {
        return Folder.getHomeFolderWithChildren();
      }

      return Folder.findByPk(newParentId)
        .then((newParent) => {
          if (!newParent) {
            return Folder.getHomeFolderWithChildren();
          }

          return newParent.addWork(work)
            .then(() => Folder.getFolderWithChildren(newParentId));
        });
    }

    // if in any other folder, must first remove from parent folder
    return Folder.findByPk(work.folderId)
      .then(currentParent => currentParent.removeWork(work)
        .then(() => {
          if (!newParentId || newParentId === HOME.ID) {
            return Folder.getHomeFolderWithChildren();
          }

          return Folder.findByPk(newParentId)
            .then((newParent) => {
              if (!newParent) {
                return Folder.getHomeFolderWithChildren();
              }

              return newParent.addWork(work)
                .then(() => Folder.getFolderWithChildren(newParentId));
            });
        }));
  });

module.exports = Work;
