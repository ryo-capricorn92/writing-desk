const Folder = require('../model/Folder');
const Work = require('../model/Work');

const { HOME } = require('../constants');

Folder.newFolder = (folder, parentId) => Folder.create(folder)
  .then((newFolder) => {
    if (!parentId || parentId === HOME.ID) {
      return newFolder;
    }

    return Folder.findByPk(parentId)
      .then((parent) => {
        if (!parent) {
          return newFolder;
        }

        return parent.addFolder(newFolder)
          .then(() => ({ ...newFolder, folderId: parent.id }));
      });
  });

Folder.getAllFolderPedigree = () => Folder.findAll()
  .then((folders) => {
    const pedigree = {
      [HOME.ID]: {
        children: [],
        id: HOME.ID,
        name: HOME.NAME,
      },
    };

    folders.forEach((folder) => {
      pedigree[folder.id] = {
        children: [],
        id: folder.id,
        name: folder.name,
      };
    });

    folders.forEach((folder) => {
      const parentId = folder.folderId || HOME.ID;

      pedigree[parentId].children.push(pedigree[folder.id]);
    });

    return pedigree[HOME.ID];
  });

const getAncestry = (parents = [], id) => Folder.findByPk(id)
  .then((folder) => {
    parents.unshift({ name: folder.name, id: folder.id });

    if (!folder.folderId) {
      return [{ id: HOME.ID, name: HOME.NAME }, ...parents];
    }

    return getAncestry(parents, folder.folderId);
  });

const derive = isFolder => f => ({
  id: f.id,
  name: f.name,
  icon: f.icon,
  isFolder,
  color: f.color,
});

Folder.getClosestIcon = folderId => Folder.findByPk(folderId)
  .then((folder) => {
    if (folder.icon || folder.color || !folder.folderId) {
      return {
        color: folder.color,
        icon: folder.icon,
      };
    }
    return Folder.getClosestIcon(folder.folderId);
  });

Folder.getHomeFolderWithChildren = () => Work.findAll({ where: { folderId: null } })
  .then(works => Folder.findAll({ where: { folderId: null } })
    .then(folders => ({ works, folders })))
  .then(({ folders, works }) => {
    const derivedFolders = folders.map(derive(true));
    const derivedWorks = works.map(derive(false));

    return {
      id: HOME.ID,
      name: HOME.NAME,
      icon: '',
      color: '',
      ancestry: [{ id: HOME.ID, name: HOME.NAME }],
      children: [...derivedFolders, ...derivedWorks],
    };
  });

Folder.getFolderWithChildren = (id) => {
  if (!id || id === HOME.ID) {
    return Folder.getHomeFolderWithChildren();
  }

  return Folder.findByPk(id)
    .then(folder => folder.getFolders()
      .then(folders => ({ folder, folders })))
    .then(({ folder, folders }) => folder.getWorks()
      .then(works => ({ folder, folders, works })))
    .then(({ folder, folders, works }) => getAncestry([], folder.id)
      .then(ancestry => ({ ancestry, folder, folders, works })))
    .then(({ ancestry, folder, folders, works }) => {
      const derivedFolders = folders.map(derive(true));
      const derivedWorks = works.map(derive(false));

      return {
        id: folder.id,
        name: folder.name,
        icon: folder.icon,
        color: folder.color,
        ancestry,
        children: [...derivedFolders, ...derivedWorks],
      };
    });
};

Folder.moveFolder = (folderId, newParentId) => Folder.findByPk(folderId)
  .then((folder) => {
    // if in HOME folder, no need to remove from current
    if (!folder.folderId) {
      if (!newParentId || newParentId === HOME.ID) {
        return Folder.getHomeFolderWithChildren();
      }

      return Folder.findByPk(newParentId)
        .then((newParent) => {
          if (!newParent) {
            return Folder.getHomeFolderWithChildren();
          }

          return newParent.addFolder(folder)
            .then(() => Folder.getFolderWithChildren(newParentId));
        });
    }

    // if in any other folder, must first remove from parent folder
    return Folder.findByPk(folder.folderId)
      .then(currentParent => currentParent.removeFolder(folder)
        .then(() => {
          if (!newParentId || newParentId === HOME.ID) {
            return Folder.getHomeFolderWithChildren();
          }

          return Folder.findByPk(newParentId)
            .then((newParent) => {
              if (!newParent) {
                return Folder.getHomeFolderWithChildren();
              }

              return newParent.addFolder(folder)
                .then(() => Folder.getFolderWithChildren(newParentId));
            });
        }));
  });

module.exports = Folder;
